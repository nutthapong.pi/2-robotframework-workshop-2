*** Settings ***
Library  BuiltIn
Library  SeleniumLibrary

*** Variables ***
${url}=  https://ptt-devpool-robotframework.herokuapp.com
${id}=  admin
${pass}=  1234

&{mydict0}=   name= nutthapong   lname= piboonvitayagul  email= nutthapong.pi@gmail.com
&{mydict1}=   name= nutthapong1   lname= piboonvitayagul1  email= nutthapong.pi1@gmail.com
&{mydict2}=   name= nutthapong2   lname= piboonvitayagul2  email= nutthapong.pi2@gmail.com
&{mydict3}=   name= nutthapong3   lname= piboonvitayagul3  email= nutthapong.pi3@gmail.com

@{listmydict}=  mydict0  mydict1  mydict2  mydict3

*** Test Cases  ***
1. เปิด Browser
    Open Browser  about:blank  browser=Chrome
    Maximize Browser Window
    Set Selenium Speed  0.5

2. ไปที่เว็บไซต์ https://ptt-devpool-robotframework.herokuapp.com
    Go To  ${url}

3. กรอกข้อมูล เพื่อ Login
    Input Text  id=txt_username  ${id}
    Input Text  id=txt_password  ${pass}

4. กดปุ่มค้นหา
    Click Button  id=btn_login

5. กดปุ่ม New Data
    Click Button  id=btn_new

6. เพิ่มข้อมูลลงฟอร์ม
    FOR  ${i}  IN  @{listmydict}
        Test  &{${i}}
        Click Button  id=btn_new
    END
    Click Button  id=btn_new_close

7. Screenshot
    Capture Page Screenshot  filename=${CURDIR}/screenshot.png


*** Keywords ***
Test
    [Arguments]    &{para}
    FOR    ${key}  ${value}   IN    &{para}
            Run Keyword If  '${key}' == 'name'   Input Text  id=txt_new_firstname  ${para["${key}"]}
            Run Keyword If  '${key}' == 'lname'   Input Text  id=txt_new_lastname  ${para["${key}"]}
            Run Keyword If  '${key}' == 'email'   Input Text  id=txt_new_email  ${para["${key}"]}
    END
    Click Button  id=btn_new_save




    
